#!/usr/bin/python2.6
# -*- coding:utf-8 -*-

import sys, os
import optparse
import re


def join(values):
    return str.join(' ', values)

def uniq(L):
    '''remove contiguous duplicates

    >>> uniq([1,1,2,2,2,3,1])
    [1, 2, 3, 1]
    >>> uniq([0,1,1,2,2,2,3,1,1])
    [0, 1, 2, 3, 1]
    '''

    if not L: return []

    retval = [L[0]]
    for i in range(1, len(L)):
        if L[i] != L[i-1]: retval.append(L[i])

    return retval



from pyarco.types import SortedDict

class UnknownRule(Exception): pass

class Rule:
    def __init__(self, **kargs):
        gen = kargs['gen']
        if isinstance(gen, str):
            self.gen = gen.split()
        elif isinstance(gen, list):
            self.gen = gen[:]
        else:
            print "gen type error"

        req = kargs.get('req', [])
        if isinstance(req, str):
            self.req = req.split()
        elif isinstance(req, list):
            self.req = req[:]
        else:
            print "req type error"

        self.cmd = kargs.get('cmd', '')

        if kargs.has_key('name'):
            self.name = kargs['name']

    def match_gen(self, fname):
        return fname in self.gen

    def get_cmd(self):
        return self.cmd.format(
            gen=join(self.gen),
            req=join(self.req))

    def __str__(self):
        return "{gen}: {req}\n\t{cmd}".format(
            gen=join(self.gen),
            req=join(self.req),
            cmd=self.get_cmd())


class Template(Rule):
    def __init__(self, **kargs):
        assert kargs.has_key('gen')
        assert kargs.has_key('req')
        assert kargs.has_key('cmd')

        for i in range(3):
            kargs['req'] = kargs['req'].replace('{%s}' % i, r'\%s' % (i+1))

        Rule.__init__(self, **kargs)
        self.req = join(self.req)

    def match_gen(self, fname):
        return re.search(self.gen, fname) is not None

    def apply(self, env, fname):
        for i in self.gen:
            self.apply_with_target(env, fname, i)

    def apply_with_target(self, env, fname, target):
        mo = re.search(target, fname)
        if mo is None: return

        rule_req = re.compile("^%s$" % target).sub(self.req, fname)
        if rule_req == fname: return

        rule = env.rule(
            gen = fname,
            req = rule_req,
            cmd = self.cmd)

        print "stem: ", mo.groups()
        print "New rule from template '{0}':\n{1}\n".format(self.name, rule)


class WakeEnviron:
    def __init__(self, wakefile):
        self.data = SortedDict()

        for m in [self.rule,
                  self.template]:
            self.data[m.__name__] = m

        self.nrule = 0
        self.rules = []
        self.templates = []

        fd = file(wakefile)
        wakecontent = fd.read()
        fd.close()

        code = compile(wakecontent, wakefile, 'exec')
        exec(code) in self.data

        #print wake_env.keys()
        #print wake_env['rule']

        self.data['build']()

    def rule(self, **kargs):
        retval = Rule(**kargs)
        retval.index = 'n{0}'.format(self.nrule)
        self.nrule += 1
        self.rules.append(retval)
        return retval

    def template(self, **kargs):
        retval = Template(**kargs)
        retval.index = 'n{0}'.format(self.nrule)
        self.nrule += 1
        self.templates.append(retval)
        return retval

    def find_rule_by_target(self, target):
        for r in self.rules:
            if r.match_gen(target):
                return r

        raise UnknownRule


    def find_rule_by_index(self, index):
        try:
            return self.rules[index]
        except IndexError:
            raise UnknownRule

    def build(self, files, level=0):
        cmds = []
        intermediate = []
        exec_rules = []
        for f in files:
            if os.path.exists(f):
                if level == 0:
                    print "wake: Nothing to be done for `{0}'".format(f)
                continue

            #apply templates
            for t in self.templates:
                t.apply(self, f)

            try:
                rule = self.find_rule_by_target(f)

                print "'{0}' depends on {1}\n--".format(f, rule.req)

                _cmds, _inter = self.build(rule.req, level+1)
                cmds.extend(_cmds + [rule.get_cmd()])
                intermediate.extend(rule.gen + _inter)

            except UnknownRule:
                print "make: *** No rule to make target `{0}'.  Stop.".format(f)
                sys.exit(2)


        if level == 0:
            intermediate = set(intermediate) - set(files)

        return uniq(cmds), intermediate


    def clean(self):
        gen = []
        for r in [x for x in self.rules if not isinstance(x, Template)]:
            gen.extend(r.gen)

        print "targets:", gen

        gen = [x for x in gen if os.path.exists(x)]

        if gen:
            print "removing files", gen
            os.system('rm {0}'.format(join(gen)))
        else:
            print "nothing to remove"


    def gen_makefile(self):
         for r in self.rules.values():
             if not r.cmd: continue
             print r
             print


parser = optparse.OptionParser()

parser.add_option('--build', action="store_true",
                  help="build phase.")

parser.add_option('--clean', action="store_true",
                  help="clean phase.")

parser.add_option('--config', action="store_true",
                  help="config phase.")

parser.add_option('-C', '--directory', dest='directory',
                  help="Change to DIRECTORY before doing anything.")

parser.add_option('-f', '--file', dest='FILE',
                  help="Read file as a wakefile.")

parser.add_option('--gen-makefile', action='store_true',
                  help="Generate equivalent makefile.")


def main():

    opts, args = parser.parse_args(sys.argv[1:])

    if opts.directory:
        os.chdir(opts.directory)

    wakefile = os.path.join(os.getcwd(), 'wakefile')
    if not os.path.exists(wakefile):
        print "wake: *** No targets specified and no wakefile found.  Stop."
        sys.exit(2)

    wake_env = WakeEnviron(wakefile)
#    print wake_env.rules

    if opts.gen_makefile:
        wake_env.gen_makefile()
        sys.exit(0)

    if opts.config:
        print 'wake: config is not implemented yet'

    if opts.build or \
            all([not x for x in [opts.config, opts.build, opts.clean]]):

        try:
            if not args:
                args = wake_env.find_rule_by_index(0).gen
        except UnknownRule:
            print "wake: No rule nor target defined"  # make message for this?
            sys.exit(1)

        cmds, intermediate = wake_env.build(args)

        if intermediate:
            print 'intermediate:\n ', str.join('\n  ', intermediate)
            cmds.append('rm {0}'.format(join(intermediate)))

        if cmds:
            print 'cmds:\n ', str.join('\n  ', cmds)
            for cmd in cmds:
                os.system(cmd)

    if opts.clean:
        wake_env.clean()


if __name__ == '__main__':
    main()


